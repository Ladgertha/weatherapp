plugins {
    id(Plugin.JAVA_LIBRARY)
    id(Plugin.KOTLIN_JVM)
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencies {
    implementation(Libraries.kotlin)

    testImplementation(Libraries.jUnit)
}
