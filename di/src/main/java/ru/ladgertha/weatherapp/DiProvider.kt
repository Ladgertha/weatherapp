package ru.ladgertha.weatherapp

import android.content.Context
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

object DiProvider {
    fun buildDi(context: Context) {
        startKoin {
            androidContext(context)
            modules(appModules)
        }
    }
}