package ru.ladgertha.weatherapp

import ru.ladgertha.weatherapp.feature_home_impl.di.homeModule

val appModules = listOf(
    homeModule
)
