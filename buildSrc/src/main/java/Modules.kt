object Modules {
    const val di = ":di"
    const val feature_home_api = ":feature-home-api"
    const val feature_home_impl = ":feature-home-impl"
}