package ru.ladgertha.weatherapp.feature_home_impl.ui

import android.content.Intent
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.ladgertha.weatherapp.feature_home_impl.R

class MainScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_screen)
    }

    companion object {
        fun openMainScreenActivity(context: Context) =
            Intent(context, MainScreenActivity::class.java)
    }
}