package ru.ladgertha.weatherapp.feature_home_impl.di

import androidx.appcompat.app.AppCompatActivity
import org.koin.dsl.module
import ru.ladgertha.weatherapp.feature_home_api.IHomeStarter
import ru.ladgertha.weatherapp.feature_home_impl.HomeStarter

val homeModule = module {
    factory<IHomeStarter> { (activity: AppCompatActivity) ->
        HomeStarter(
            activity
        )
    }
}