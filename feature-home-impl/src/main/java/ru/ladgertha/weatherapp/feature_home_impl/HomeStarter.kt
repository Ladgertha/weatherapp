package ru.ladgertha.weatherapp.feature_home_impl

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import ru.ladgertha.weatherapp.feature_home_api.IHomeStarter
import ru.ladgertha.weatherapp.feature_home_impl.ui.MainScreenActivity

class HomeStarter(private val activity: AppCompatActivity) : IHomeStarter {
    override fun start() {
        MainScreenActivity.openMainScreenActivity(activity).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            activity.startActivity(this)
        }
    }
}