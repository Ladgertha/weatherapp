import org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions

plugins {
    id(Plugin.ANDROID_LIBRARY)
    id(Plugin.KOTLIN_ANDROID)
}

android {
    compileSdkVersion(AppData.compileSdkVersion)

    defaultConfig {
        minSdkVersion(AppData.minSdkVersion)
        targetSdkVersion(AppData.targetSdkVersion)

        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = AppData.testInstrumentationRunner
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true

            proguardFiles("proguard-android-optimize.txt", "proguard-rules.pro")
        }
    }

    kotlinOptions {
        val options = this as? KotlinJvmOptions
        options?.jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

}

dependencies {
    implementation(project(Modules.feature_home_api))
    implementation(Libraries.appCompat)
    implementation(Libraries.lifecycle)

    // KOIN
    implementation(Libraries.koin)
    implementation(Libraries.kotlin)

    testImplementation(Libraries.jUnit)
}