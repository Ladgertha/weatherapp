package ru.ladgertha.weatherapp.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import pl.droidsonroids.gif.GifDrawable
import ru.ladgertha.weatherapp.R
import ru.ladgertha.weatherapp.feature_home_api.IHomeStarter

private const val LOOP_NUMBER = 5
private const val LOOP_NUMBER_LISTENER = LOOP_NUMBER - 1

class MainActivity : AppCompatActivity() {

    private val homeStarter: IHomeStarter by inject {
        parametersOf(this@MainActivity)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val gifDrawable = GifDrawable(resources, R.drawable.splash)
        splash.setImageDrawable(gifDrawable)
        gifDrawable.loopCount = LOOP_NUMBER

        gifDrawable.addAnimationListener {
            if (it == LOOP_NUMBER_LISTENER) {
                homeStarter.start()
            }
        }
    }
}
