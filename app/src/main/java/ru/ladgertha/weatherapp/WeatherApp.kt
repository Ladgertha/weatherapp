package ru.ladgertha.weatherapp

import android.app.Application
import android.content.Context

class WeatherApp: Application() {
    override fun onCreate() {
        super.onCreate()
        initDi(this)
    }

    private fun initDi(context: Context) {
        DiProvider.buildDi(context)
    }
}